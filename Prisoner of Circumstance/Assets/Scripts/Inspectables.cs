﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Inspectables : MonoBehaviour
    
{
    public Vector2 widthThreshold;
    public Vector2 heightThreshold;

    public bool canInspect;
    public bool inspected;
    public GameObject player;
    public FirstPersonController thePlayer;
    public float speed;
    public Quaternion originalRotation;
    public Vector3 originalPosition;
    public GameObject inspectPoint;
    public Camera mainCamera;
    public GameObject theCharacter;
    public Transform thisOne;
    public Transform playerPosition;
    

    // Start is called before the first frame update
    void Start()
    {
       gameObject.GetComponent<BoxCollider>().enabled = false;
       player = GameObject.Find("FPSController");
        originalRotation = transform.rotation;
        originalPosition = transform.position;
        theCharacter = GameObject.Find("FirstPersonCharacter");
        mainCamera = theCharacter.GetComponent<Camera>();
        inspectPoint = GameObject.Find("Inspect Point");
        thisOne = gameObject.GetComponent<Transform>();
        playerPosition = player.GetComponent<Transform>();
        
        thePlayer = player.GetComponent<FirstPersonController>();
        //theCharacter = GameObject.Find("FirstPersonCharacter");
        speed = 1f;
    }

    // Update is called once per frame
    void Update()
    {
       float distance = Vector3.Distance(thisOne.transform.position, playerPosition.transform.position);
        
        if (distance < 5f)
        {
            gameObject.GetComponent<BoxCollider>().enabled = true;
        }
        else
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
        if (Input.GetKeyDown(KeyCode.E) && canInspect)
        {
            thePlayer.inspecting = true;
            inspected = true;
            canInspect = false;
        }
        else if (Input.GetKeyDown(KeyCode.E) && inspected)
        {
            //transform.Rotate((Vector3.zero) * speed);
            transform.position = originalPosition;
            transform.rotation = originalRotation;
            thePlayer.inspecting = false;
            inspected = false;
            gameObject.GetComponent<BoxCollider>().enabled = true;
            
        }

        if (inspected)
        {

            transform.position = new Vector3(inspectPoint.transform.position.x, inspectPoint.transform.position.y, inspectPoint.transform.position.z);
            if (Input.GetAxis("Mouse X") < 0)
            {
                transform.Rotate((Vector3.forward) * speed);
            }

            if (Input.GetAxis("Mouse X") > 0)
            {
                transform.Rotate((Vector3.forward) * -speed);
            }

            if (Input.GetAxis("Mouse Y") < 0)
            {
                transform.Rotate((Vector3.right) * speed);
            }

            if (Input.GetAxis("Mouse Y") > 0)
            {
                transform.Rotate((Vector3.right) * -speed);
            }

            gameObject.GetComponent<BoxCollider>().enabled = false;

            //transform.Rotate(new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * Time.deltaTime);
        }
    }


    void OnTriggerEnter(Collider Other)
    {
        if (Other.gameObject.tag == "Player" && !canInspect) {
            canInspect = true;
        }
    }

    void OnTriggerExit(Collider Other)
    { 
        if (Other.gameObject.tag == "Player")
        {
            canInspect = false;
        }
    }

    /*void OnBecameVisible()
        {
            if (!inspected)
            {
                gameObject.GetComponent<BoxCollider>().enabled = true;
            
            }

        }

    void OnBecameInvisible()
    {
        gameObject.GetComponent<BoxCollider>().enabled = false;
        Debug.Log ("Can't see it!");
    } */   
}
