﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Door : MonoBehaviour
{
    public Animation anim;
    public bool canOpen;
    public bool isOpened;
    public bool canClose;
    

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animation>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && canOpen && !isOpened)
        {
            anim.Play("Door Open");
            isOpened = true;
            //canClose = true;
        }

        if (Input.GetKeyDown(KeyCode.E) && canClose && isOpened)
        {
            
            anim.Play("Door Close");
            canClose = false;
            isOpened = false;
        }
    }

    void OnTriggerEnter(Collider Other)
    {
        if (!isOpened)
        {
            canOpen = true;

        }
        else if (isOpened)
        {
            canClose = true;
        }
    }

    void OnTriggerExit(Collider Other) 
    {
        canOpen = false;    
    
    }
} 